# GTK Tutorial
## History GNOME, GIMP & GTK Community
### Origins
### Community governance
### How to contribute
## GLib/GObject/GIO Overview
Think of GLib as a
GLib project provides three libraries:
-  Portable C runtime. It provides basic memory handling, some portable synchronous I/O APIs, advanced data structures and more.
-  GObject a library that provdes a type system and an object model with a refcounted memory management model for C
-  GIO a collection of APIs on top of GObject to provide async I/O abstractions (fs, networking, settings, IPC...)
### GLib
We are going to go through the basics of how to build a GLib application in C as well as using some of its basic data structures.
#### pkg-config
Before we start, a few minutes to explain how pkg-config works.
pkg-config is a tool that allows us to abstract ourselves from the C flags, include libraries and other framework specific data.

```
$ pkg-config --cflags --libs glib-2.0
$ cat /usr/lib64/pkgconfig/glib-2.0.pc
prefix=/usr
libdir=${prefix}/lib64
includedir=${prefix}/include

bindir=${prefix}/bin
glib_genmarshal=${bindir}/glib-genmarshal
gobject_query=${bindir}/gobject-query
glib_mkenums=${bindir}/glib-mkenums

Name: GLib
Description: C Utility Library
Version: 2.64.1
Requires.private: libpcre >=  8.31
Libs: -L${libdir} -lglib-2.0
Libs.private: -pthread
Cflags: -I${includedir}/glib-2.0 -I${libdir}/glib-2.0/include
```
#### Hello World/Data Structures
```
#include <glib.h>

gint
main (gint argc, gchar** argv) {
  GHashTable *table = g_hash_table_new(g_str_hash, g_str_equal);
  g_hash_table_insert(table, "Hello", "World");
  
  g_print("%s %s\n","Hello", g_hash_table_lookup(table, "Hello"));
  
  g_hash_table_destroy(table);
  return 0;
}
```
```
$ gcc `pkg-config --libs --cflags glib-2.0` 00_hello_world.c -o hello_world 
$ ./hello_world
Hello World!
```
#### DevHelp
-  Screenshot of DevHelp showing GLib vast utility chest
#### Coding style
lowercase and underscore, space
#### Event Loop
##### timeout
timeouts are events in the timeloop we can install
and they call a callback
(look at definition in devhelp)
##### idle
idles are scheduled for the next interation of the loop instead
of a time bound period
#### logging
### GObject
#### Instance/Casting
#### Ref counting

#### Inheritance
#### Private data
#### Properties
#### Signals
#### Interfaces

### GIO
#### GApplication
## GObject Introspection
### Vala
### PyGObject
### Gjs
## GTK 2/3 Basics
### GTK Window
### GTK Button/Callbacks
### Handle input
-  Clicks & Gestures?
### GtkBuilder / (Glade????)
### GTK 3 CSS Theming
### GTK 3 Inspector
### Custom Widgets in GTK2
### Custom Widgets in GTK3
## i18n/l10n
## Input Methods
